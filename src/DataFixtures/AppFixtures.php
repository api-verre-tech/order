<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Order;
use App\Entity\OrderDetails;
use App\Entity\OrderState;
use App\Utils\CallAPI;

use \DateTime;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Création d'une commande
        $order = new Order();
        $order->setFkidU(1); // Changer en appel API
        $order->setStatus(OrderState::Preparation);
        $manager->persist($order);
        $manager->flush(); // On flush pour avoir l'id de la commande sauvegarder

        // Details de la commande
        $orderDetails = new OrderDetails();
        $orderDetails->setFkidC($order);

        $orderDetails->setFkidP(5);
        $orderDetails->setQuantity(2);
        $manager->persist($orderDetails);

        $orderDetails = new OrderDetails();
        $orderDetails->setFkidC($order);

        $orderDetails->setFkidP(3);
        $orderDetails->setQuantity(2);
        $manager->persist($orderDetails);

        
        // Création d'une commande
        $order = new Order();
        $order->setFkidU(3); // Changer en appel API
        $order->setStatus(OrderState::Preparation);
        $manager->persist($order);
        $manager->flush(); // On flush pour avoir l'id de la commande sauvegarder

        // Details de la commande
        $orderDetails = new OrderDetails();
        $orderDetails->setFkidC($order);

        $orderDetails->setFkidP(1);
        $orderDetails->setQuantity(5);
        $manager->persist($orderDetails);

        $orderDetails = new OrderDetails();
        $orderDetails->setFkidC($order);

        $orderDetails->setFkidP(2);
        $orderDetails->setQuantity(1);
        $manager->persist($orderDetails);

        
        // Création d'une commande
        $order = new Order();
        $order->setFkidU(2); // Changer en appel API
        $order->setStatus(OrderState::Preparation);
        $manager->persist($order);
        $manager->flush(); // On flush pour avoir l'id de la commande sauvegarder

        // Details de la commande
        $orderDetails = new OrderDetails();
        $orderDetails->setFkidC($order);

        $orderDetails->setFkidP(1);
        $orderDetails->setQuantity(5);
        $manager->persist($orderDetails);

        $orderDetails = new OrderDetails();
        $orderDetails->setFkidC($order);

        $orderDetails->setFkidP(2);
        $orderDetails->setQuantity(1);
        $manager->persist($orderDetails);

        
        // Création d'une commande
        $order = new Order();
        $order->setFkidU(2); // Changer en appel API
        $order->setStatus(OrderState::Preparation);
        $manager->persist($order);
        $manager->flush(); // On flush pour avoir l'id de la commande sauvegarder

        // Details de la commande
        $orderDetails = new OrderDetails();
        $orderDetails->setFkidC($order);

        $orderDetails->setFkidP(1);
        $orderDetails->setQuantity(5);
        $manager->persist($orderDetails);

        $orderDetails = new OrderDetails();
        $orderDetails->setFkidC($order);

        $orderDetails->setFkidP(2);
        $orderDetails->setQuantity(1);
        $manager->persist($orderDetails);

        $manager->flush();
    }
}
