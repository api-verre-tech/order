<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use DateInterval;
use DateTime;
use DateTimeZone;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;

abstract class OrderState
{
    const Preparation = "En préparation";
    const Available = "Disponible";
    const Terminated = "Terminé";
}

/**
 * @ApiResource(
 *     collectionOperations={"post", "get"},
 *     itemOperations={
 *         "delete",
 *         "get",
 *         "patch"
 *     }
 * )
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "fkid_u": "end"})
 * @ApiFilter(DateFilter::class, properties={"date_reservation_start", "date_reservation_end"})
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Positive
     * @Assert\NotNull
     */
    private $fkid_u;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\Type("\DateTimeInterface")
     */
    private $date_reservation_start;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\Type("\DateTimeInterface")
     */
    private $date_reservation_end;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity=OrderDetails::class, mappedBy="fkid_c", orphanRemoval=true)
     */
    private $OrderDetails;

    public function __construct()
    {
        $this->OrderDetails = new ArrayCollection();
        $this->date_reservation_start = new DateTime('NOW', new DateTimeZone("+0200"));
        $this->date_reservation_end = new DateTime('NOW', new DateTimeZone("+0200"));
        $this->date_reservation_end->add(new DateInterval("P2D"));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFkidU(): ?int
    {
        return $this->fkid_u;
    }

    public function setFkidU(int $fkid_u): self
    {
        $this->fkid_u = $fkid_u;

        return $this;
    }

    public function getDateReservationStart(): ?\DateTimeInterface
    {
        return $this->date_reservation_start;
    }

    public function getDateReservationEnd(): ?\DateTimeInterface
    {
        return $this->date_reservation_end;
    }

    public function setDateReservationStart(\DateTime $date_reservation_start): self
    {
        $this->date_reservation_start = $date_reservation_start;

        return $this;
    }

    public function setDateReservationEnd(\DateTime $date_reservation_end): self
    {
        $this->date_reservation_end = $date_reservation_end;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|OrderDetails[]
     */
    public function getOrderDetails(): Collection
    {
        return $this->OrderDetails;
    }

    public function addOrderDetail(OrderDetails $OrderDetail): self
    {
        if (!$this->OrderDetails->contains($OrderDetail)) {
            $this->OrderDetails[] = $OrderDetail;
            $OrderDetail->setFkidC($this);
        }

        return $this;
    }

    public function removeOrderDetail(OrderDetails $OrderDetail): self
    {
        if ($this->OrderDetails->removeElement($OrderDetail)) {
            // set the owning side to null (unless already changed)
            if ($OrderDetail->getFkidC() === $this) {
                $OrderDetail->setFkidC(null);
            }
        }

        return $this;
    }
}
