<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\OrderDetailsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={"post"},
 *     itemOperations={"delete","get"}
 * )
 * @ORM\Entity(repositoryClass=OrderDetailsRepository::class)
 */
class OrderDetails
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Positive
     * @Assert\NotNull
     * @Groups({"order:read"})
     */
    private $fkid_p;

    /**
     * @ORM\ManyToOne(targetEntity=Order::class, inversedBy="OrderDetails")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fkid_c;

    /**
     * @ORM\Column(type="integer")
     * @Assert\PositiveOrZero
     * @Groups({"order:read"})
     */
    private $quantity;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFkidP(): ?int
    {
        return $this->fkid_p;
    }

    public function setFkidP(int $fkid_p): self
    {
        $this->fkid_p = $fkid_p;

        return $this;
    }

    public function getFkidC(): ?Order
    {
        return $this->fkid_c;
    }

    public function setFkidC(?Order $fkid_c): self
    {
        $this->fkid_c = $fkid_c;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }
}
